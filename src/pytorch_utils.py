import math

import numpy as np
import six

""" read file in cff format

    the contents of such files look like this:
    133
    75
    0
    0 0 1 4 2 0 0
    0
    0 1 4 2 3 1 0

    the first two lines represent TODO
"""
def readFile_cff(filepath):
    lines = []
    with open(filepath) as file_in:
        for line in file_in:
            lines.append(line)
    inputSize = int(lines[0].strip())
    outputSize = int(lines[1].strip())
    items_labels = [x.strip() for x in lines[2:]]
    items = items_labels[1::2]
    labels = items_labels[::2]
    return items, labels, inputSize, outputSize


#flatten a list
def flatten(xss):
    return [x for xs in xss for x in xs]


#find how many classes we have in data:
def find_unique_classes(*inputlists):
    unique_classes = np.unique(flatten([*inputlists])).tolist()
    unique_classes = [str(x) for x in unique_classes]
    number_of_unique_classes = len(unique_classes)
    return unique_classes, number_of_unique_classes


def find_unique_symbols(*inputlists):
    flat_list = flatten([*inputlists])
    maxlen = len(max(flat_list, key=len))
    unique_elem_list = []
    for elem in flat_list:
        tokens = [x for x in elem.split(' ')]
        for t in tokens:
            if not t in unique_elem_list:
                unique_elem_list.append(t)
    return maxlen, unique_elem_list + [' ']


def pad_sequences(sequences, maxlen=None, dtype='int32',
                  padding='pre', truncating='pre', value=0.):
    if not hasattr(sequences, '__len__'):
        raise ValueError('`sequences` must be iterable.')
    lengths = []
    for x in sequences:
        if not hasattr(x, '__len__'):
            raise ValueError('`sequences` must be a list of iterables. '
                             'Found non-iterable: ' + str(x))
        lengths.append(len(x))

    num_samples = len(sequences)
    #print("NUM SAMPLES", num_samples)
    if maxlen is None:
        maxlen = np.max(lengths)

    # take the sample shape from the first non empty sequence
    # checking for consistency in the main loop below.
    sample_shape = tuple()
    for s in sequences:
        if len(s) > 0:
            sample_shape = np.asarray(s).shape[1:]
            break

    is_dtype_str = np.issubdtype(dtype, np.str_) or np.issubdtype(dtype, np.unicode_)
    if isinstance(value, six.string_types) and dtype != object and not is_dtype_str:
        raise ValueError("`dtype` {} is not compatible with `value`'s type: {}\n"
                         "You should set `dtype=object` for variable length strings."
                         .format(dtype, type(value)))

    x = np.full((num_samples, maxlen) + sample_shape, value, dtype=dtype)
    for idx, s in enumerate(sequences):
        if not len(s):
            continue  # empty list/array was found
        if truncating == 'pre':
            trunc = s[-maxlen:]
        elif truncating == 'post':
            trunc = s[:maxlen]
        else:
            raise ValueError('Truncating type "%s" '
                             'not understood' % truncating)

        # check `trunc` has expected shape
        trunc = np.asarray(trunc, dtype=dtype)
        if trunc.shape[1:] != sample_shape:
            raise ValueError('Shape of sample %s of sequence at position %s '
                             'is different from expected shape %s' %
                             (trunc.shape[1:], idx, sample_shape))

        if padding == 'post':
            x[idx, :len(trunc)] = trunc
        elif padding == 'pre':
            x[idx, -len(trunc):] = trunc
        else:
            raise ValueError('Padding type "%s" not understood' % padding)
    return x


def to_categorical(y, num_classes=None, dtype='float32'):
    y = np.array(y, dtype='int')
    input_shape = y.shape
    if input_shape and input_shape[-1] == 1 and len(input_shape) > 1:
        input_shape = tuple(input_shape[:-1])
    y = y.ravel()
    if not num_classes:
        num_classes = np.max(y) + 1
    n = y.shape[0]
    categorical = np.zeros((n, num_classes), dtype=dtype)
    categorical[np.arange(n), y] = 1
    output_shape = input_shape + (num_classes,)
    categorical = np.reshape(categorical, output_shape)
    return categorical


def make_pytorch_dicts(*input_paths):
    input_paths = [x for x in input_paths]
    item_lsts = []
    label_lsts = []
    for inpath in input_paths:
        items_list, labels_list, _, _ = readFile_cff(inpath)
        item_lsts.append(items_list)
        label_lsts.append(labels_list)
    items_list = flatten(item_lsts)
    labels_list = flatten(label_lsts)

    classes, n_classes = find_unique_classes(labels_list) # possible moves
    maxlen, all_symbols = find_unique_symbols(items_list) # basically, the number of POS tags
    n_symbols = len(all_symbols)

    #print('Number of classes: ', n_classes)
    #print('Number of symbols: ', n_symbols)
    #print('Max length of sequence: ', maxlen)

    symbol_to_idx = {s: n for n, s in enumerate(all_symbols)}
    idx_to_symbol = {n: s for n, s in enumerate(all_symbols)}

    class_to_idx = {c: n for n, c in enumerate(classes)}
    idx_to_class = {n: c for n, c in enumerate(classes)}

    return n_classes, maxlen, n_symbols, symbol_to_idx, idx_to_symbol, class_to_idx, idx_to_class, classes

def encode_x_batch(x_batch, symbol_to_idx, n_symbols):
    return pad_sequences([encode_x(x, symbol_to_idx, n_symbols) for x in x_batch],maxlen=15) # TODO read maxlen from the file


def encode_x(x, symbol_to_idx, n_symbols):
    idx_x = [symbol_to_idx[s] for s in x]
    return to_categorical(idx_x, num_classes=n_symbols)


def encode_y_batch(y_batch, class_to_idx, n_classes):
    return np.array([encode_y(y, class_to_idx, n_classes) for y in y_batch])


def encode_y(y, class_to_idx, n_classes):
    idx_y = class_to_idx[y]
    return to_categorical(idx_y, num_classes=n_classes)


def preprocess_data(items_list, labels_list, batch_size, symbol_to_idx, class_to_idx, n_symbols, n_classes):
    upp_bound = int(math.ceil(len(items_list) / batch_size))
    data_batches = []
    i = 0
    for i in range(upp_bound -1):
        a = i * batch_size
        b = (i + 1) * batch_size
        data_batches.append((encode_x_batch(items_list[a:b], symbol_to_idx, n_symbols), encode_y_batch(labels_list[a:b], class_to_idx, n_classes)))
        i+=1
    return data_batches


def decode_x(x, idx_to_symbol):
    x = x[np.sum(x, axis=1) > 0]    # remove padding
    return ''.join([idx_to_symbol[pos] for pos in np.argmax(x, axis=1)])

def decode_y(y, idx_to_class):
    return idx_to_class[np.argmax(y)]

def decode_x_batch(x_batch, idx_to_symbol):
    return [decode_x(x, idx_to_symbol) for x in x_batch]

def decode_y_batch(y_batch, idx_to_class):
    return [idx_to_class[pos] for pos in np.argmax(y_batch, axis=1)]