import sys
import Oracle
from Dicos import Dicos
from Config import Config
from Word import Word
from Mcd import Mcd
from Moves import Moves
from FeatModel import FeatModel
import torch

import numpy as np
from pytorch_utils import *



def prepareWordBufferForDecode(buffer):
    """Add to every word of the buffer features GOVREF and LABELREF.

    GOVEREF is a copy of feature GOV and LABELREF a copy of LABEL
    GOV and LABEL are set to initialization values
    """
    for word in buffer.array:
        word.setFeat('GOV', str(Word.invalidGov()))
        word.setFeat('LABEL', Word.invalidLabel())


verbose = True
if len(sys.argv) != 9 :
    print('usage:', sys.argv[0], 'mcf_file model_file dicos_file feat_model mcd_file words_limit train_file dev_file')
    exit(1)

mcf_file =       sys.argv[1]
model_file =     sys.argv[2]
dicos_file =     sys.argv[3]
feat_model =     sys.argv[4]
mcd_file =       sys.argv[5]
wordsLimit = int(sys.argv[6])
train_fr_file = sys.argv[7]
dev_fr_file = sys.argv[8]

##########################################################################

n_classes, maxlen, n_symbols, symbol_to_idx, idx_to_symbol, class_to_idx, idx_to_class, _ = make_pytorch_dicts(dev_fr_file, train_fr_file)
##########################################################################
    
#print('reading mcd from file : ', mcd_file) # PGLE.mcd

mcd = Mcd(mcd_file) # MCD = multi column descriptions



#print('loading dicos from : ', dicos_file) # PLE.dic
dicos = Dicos(fileName=dicos_file) # dictionaries with class labels for columns from mcd

moves = Moves(dicos)

#print('reading feature model from file : ', feat_model) # basic.fm

featModel = FeatModel(feat_model, dicos)

#print('loading pytorch model from :', model_file) # /home/taniabladier/Programming/AMU/tbp/expe/out/fr.pytorch


############################

"""
load saved pytorch model
"""

batch_size = 32


# Setup the RNN and training settings
import torch.nn as nn
import torch.nn.functional as F

class SimpleLSTM(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super().__init__()
        self.lstm = torch.nn.LSTM(input_size, hidden_size, batch_first=True)
        self.linear = torch.nn.Linear(hidden_size, output_size)

    def forward(self, x):
        h = self.lstm(x)[0]
        x = self.linear(h)
        return x

    def get_states_across_time(self, x):
        h_c = None
        h_list, c_list = list(), list()
        with torch.no_grad():
            for t in range(x.size(1)):
                h_c = self.lstm(x[:, [t], :], h_c)[1]
                h_list.append(h_c[0])
                c_list.append(h_c[1])
            h = torch.cat(h_list)
            c = torch.cat(c_list)
        return h, c
    



input_size  = 133 #n_symbols
hidden_size = 128
output_size = 75 #n_classes


print('input output', input_size, output_size)
model       = SimpleLSTM(input_size, hidden_size, output_size)
criterion   = torch.nn.CrossEntropyLoss()
optimizer   = torch.optim.RMSprop(model.parameters(), lr=0.001)


checkpoint = torch.load(model_file, map_location=torch.device('cpu'), weights_only=False)
model.load_state_dict(checkpoint['model_state_dict'])
optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
epoch = checkpoint['epoch']
loss = checkpoint['loss']

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

model = model.to(device)
model.eval();


############################


#model = load_model(model_file)

#inputSize = featModel.getInputSize()
#outputSize = moves.getNb()

c = Config(mcf_file, mcd, dicos)



numSent = 0
verbose = True
numWords = 0


while c.getBuffer().readNextSentence()  and numWords < wordsLimit :
    c.getStack().empty()
    prepareWordBufferForDecode(c.getBuffer())

    numWords += c.getBuffer().getLength()

    while True :
        featVec = c.extractFeatVec(featModel)
        inputVector = featModel.buildInputVector(featVec, dicos)

        ###############
        inputVector = ' '.join(str(x) for x in inputVector)
        inputVector = encode_x_batch([inputVector], symbol_to_idx, 133) #n_symbols)
        inputVector = torch.from_numpy(inputVector).float().to(device)      
       

        with torch.no_grad():
            output = model(inputVector)

            # Pick only the output corresponding to last sequence element (input is pre padded)
            output = output[:, -1, :]
            preds = output.argmax(dim=1)
            mvt_Code = int([idx_to_class[y.item()] for y in preds][0])


        ##################
        
        #mvt_Code = outputVector.argmax()
        #mvt = moves.mvtDecode(mvt_Code)

        mvt = moves.mvtDecode(mvt_Code)

        verbose = True

        if(verbose == True) :

            """
            print("\n\n------------------------------------------\n")
            print('inputVector ::', featModel.buildInputVector(featVec, dicos))

            print('mvt code: ', mvt_Code, ' move ::: ', mvt)

            c.affiche()            

            print('predicted move: ', mvt[0], mvt[1], 'for', str(featVec))        
            """

        res = c.applyMvt(mvt) #result, True or False
        #print("RES", res)
        if not res :
            print("cannot apply predicted movement\n")
            mvt_type = mvt[0]
            mvt_label = mvt[1]
            if mvt_type != "SHIFT" :
                print("try to force SHIFT\n")
                res = c.shift()
                if res == False :
                    print("try to force REDUCE\n")
                    res = c.red()
                    if res == False :
                        print("abort sentence\n")
                        break
        if(c.isFinal()):
            break
    for i in range(1, c.getBuffer().getLength()):
        w = c.getBuffer().getWord(i)
        w.affiche(mcd)

    numSent += 1
#    if numSent % 10 == 0:
#        print ("Sent : ", numSent)






