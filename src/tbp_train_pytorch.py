import sys
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from pytorch_utils import *
from plot_lib import *
import os

"""## 1. Reading Data Files"""

"""
def readData(dataFilename) :
    allX = []
    allY = []
    try:
#        dataFile = open(dataFilename, encoding='utf-8')
        dataFile = open(dataFilename)
    except IOError:
        print(dataFilename, " : ce fichier n'existe pas")
        exit(1)


        
    inputSize = int(dataFile.readline())
    print("input size = ", inputSize)
    outputSize = int(dataFile.readline())
    print("output size = ", outputSize)

    inputLine = True # indicates whether the current line is an input line (configuration) or an output line (movement)
    for ligne in dataFile:
#        print(ligne)
        vector = ligne.split()
        vector[:] = list(map(int, vector))
        if inputLine == True:
            #print("input ", vector)
            allX.append(vector)
            inputLine = False
        else:
            #print("output ", vector)
            allY.append(vector)
            inputLine = True
    # x_train and y_train are Numpy arrays 
    x_train = np.array(allX)
    y_train = np.array(allY)
    return (inputSize, outputSize, x_train, y_train)
"""



if len(sys.argv) < 3 :
    print('usage:', sys.argv[0], 'cffTrainFileName cffDevFileName pytorchModelFileName')
    exit(1)

cffTrainFileName =   sys.argv[1]
cffDevFileName =     sys.argv[2]
pytorchModelFileName = sys.argv[3]


batch_size = 32


n_classes, maxlen, n_symbols, symbol_to_idx, idx_to_symbol, \
      class_to_idx, idx_to_class, classes = make_pytorch_dicts(cffTrainFileName, cffDevFileName)


train_items_list, train_labels_list, train_inputSize, train_outputSize  = readFile_cff(cffTrainFileName)
dev_items_list, dev_labels_list, dev_inputSize, dev_outputSize  = readFile_cff(cffDevFileName)


train_data_gen = preprocess_data(train_items_list[:800000], train_labels_list[:800000], batch_size, symbol_to_idx, class_to_idx, train_inputSize, train_outputSize)#, n_symbols, n_classes)
dev_data_gen  = preprocess_data(dev_items_list[:200000], dev_labels_list[:200000], batch_size, symbol_to_idx, class_to_idx, train_inputSize, train_outputSize)#, n_symbols, n_classes)


"""## 2. Defining the Model"""


# Set the random seed for reproducible results
torch.manual_seed(1)

# Setup the RNN and training settings
input_size  = 133 #train_inputSize #n_symbols
hidden_size = 128
output_size = 75 #train_outputSize #n_classes

class SimpleMLP(nn.Module):
    def __init__(self, input_size, output_size):
        super(SimpleMLP, self).__init__()
        self.fc1 = nn.Linear(input_size, 200)  # Dense layer with 128 units
        self.dropout = nn.Dropout(0.5)  # Dropout with 0.4 probability
        self.fc2 = nn.Linear(200, output_size)  # Dense layer with outputSize units

    def forward(self, x):
        x = F.relu(self.fc1(x))  # Apply ReLU activation
        x = self.dropout(x)  # Apply Dropout
        x = F.softmax(self.fc2(x), dim=1)  # Apply Softmax activation
        return x

    def get_states_across_time(self, x):
        h_c = None
        h_list, c_list = list(), list()
        with torch.no_grad():
            for t in range(x.size(1)):
                h_c = self.lstm(x[:, [t], :], h_c)[1]
                h_list.append(h_c[0])
                c_list.append(h_c[1])
            h = torch.cat(h_list)
            c = torch.cat(c_list)
        return h, c


class SimpleRNN(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        # This just calls the base class constructor
        super().__init__()
        # Neural network layers assigned as attributes of a Module subclass
        # have their parameters registered for training automatically.
        self.rnn = torch.nn.RNN(input_size, hidden_size, nonlinearity='relu', batch_first=True)
        self.linear = torch.nn.Linear(hidden_size, output_size)

    def forward(self, x):
        # The RNN also returns its hidden state but we don't use it.
        # While the RNN can also take a hidden state as input, the RNN
        # gets passed a hidden state initialized with zeros by default.
        h = self.rnn(x)[0]
        x = self.linear(h)
        return x

class SimpleLSTM(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super().__init__()
        self.lstm = torch.nn.LSTM(input_size, hidden_size, batch_first=True)
        self.linear = torch.nn.Linear(hidden_size, output_size)

    def forward(self, x):
        h = self.lstm(x)[0]
        x = self.linear(h)
        return x

    def get_states_across_time(self, x):
        h_c = None
        h_list, c_list = list(), list()
        with torch.no_grad():
            for t in range(x.size(1)):
                h_c = self.lstm(x[:, [t], :], h_c)[1]
                h_list.append(h_c[0])
                c_list.append(h_c[1])
            h = torch.cat(h_list)
            c = torch.cat(c_list)
        return h, c
    

"""## 3. Defining the Training Loop"""

def train(model, train_data_gen, criterion, optimizer, device):
    # Set the model to training mode. This will turn on layers that would
    # otherwise behave differently during evaluation, such as dropout.
    model.train()

    # Store the number of sequences that were classified correctly
    num_correct = 0

    # Iterate over every batch of sequences. Note that the length of a data generator
    # is defined as the number of batches required to produce a total of roughly 1000
    # sequences given a batch size.
    for batch_idx in range(len(train_data_gen)):


        # Request a batch of sequences and class labels, convert them into tensors
        # of the correct type, and then send them to the appropriate device.
        data, target = train_data_gen[batch_idx]

        data, target = torch.from_numpy(data).float().to(device), torch.from_numpy(target).long().to(device)


        # Perform the forward pass of the model
        output = model(data)  # Step ①

        # Pick only the output corresponding to last sequence element (input is pre padded)
        output = output[:, -1, :]

        # Compute the value of the loss for this batch. For loss functions like CrossEntropyLoss,
        # the second argument is actually expected to be a tensor of class indices rather than
        # one-hot encoded class labels. One approach is to take advantage of the one-hot encoding
        # of the target and call argmax along its second dimension to create a tensor of shape
        # (batch_size) containing the index of the class label that was hot for each sequence.
        target = target.argmax(dim=1)

        loss = criterion(output, target)  # Step ②

        # Clear the gradient buffers of the optimized parameters.
        # Otherwise, gradients from the previous batch would be accumulated.
        optimizer.zero_grad()  # Step ③

        loss.backward()  # Step ④

        optimizer.step()  # Step ⑤

        y_pred = output.argmax(dim=1)
        num_correct += (y_pred == target).sum().item()

    return num_correct, loss.item()

"""## 4. Defining the Testing Loop"""

def test(model, test_data_gen, criterion, device):
    # Set the model to evaluation mode. This will turn off layers that would
    # otherwise behave differently during training, such as dropout.
    model.eval()

    # Store the number of sequences that were classified correctly
    num_correct = 0

    # A context manager is used to disable gradient calculations during inference
    # to reduce memory usage, as we typically don't need the gradients at this point.
    with torch.no_grad():
        for batch_idx in range(len(test_data_gen)):
            data, target = test_data_gen[batch_idx]

            data, target = torch.from_numpy(data).float().to(device), torch.from_numpy(target).long().to(device)
            output = model(data)
            # Pick only the output corresponding to last sequence element (input is pre padded)
            output = output[:, -1, :]

            target = target.argmax(dim=1)
            loss = criterion(output, target)

            y_pred = output.argmax(dim=1)
            num_correct += (y_pred == target).sum().item()

    return num_correct, loss.item()

"""## 5. Putting it All Together"""

import matplotlib.pyplot as plt
from plot_lib import set_default, plot_state, print_colourbar

set_default()

def check_point(model, filename, epochs, optimizer, criterion):
    #torch.save(model.state_dict(), filename)

    print(f"Saving model from epoch", epochs)
    torch.save({
                'epoch': epochs,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                'loss': criterion,
                }, filename)


def resume(model, filename):
    #model.load_state_dict(torch.load(filename))

    checkpoint = torch.load(filename, map_location=torch.device('cpu'), weights_only=False)
    model.load_state_dict(checkpoint['model_state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    epoch = checkpoint['epoch']
    loss = checkpoint['loss']

def train_and_test(model, train_data_gen, test_data_gen, criterion, optimizer, max_epochs, verbose=True):
    # Automatically determine the device that PyTorch should use for computation
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    # Move model to the device which will be used for train and test
    model.to(device)

    # Track the value of the loss function and model accuracy across epochs
    history_train = {'loss': [], 'acc': []}
    history_test = {'loss': [], 'acc': []}


    early_stop_thresh = 5
    best_accuracy = -1
    best_epoch = -1
    for epoch in range(max_epochs):
        # Run the training loop and calculate the accuracy.
        # Remember that the length of a data generator is the number of batches,
        # so we multiply it by the batch size to recover the total number of sequences.
        num_correct, loss = train(model, train_data_gen, criterion, optimizer, device)
        accuracy = float(num_correct) / (len(train_data_gen) * batch_size) * 100
        history_train['loss'].append(loss)
        history_train['acc'].append(accuracy)

        # Do the same for the testing loop
        num_correct, loss = test(model, test_data_gen, criterion, device)
        accuracy = float(num_correct) / (len(test_data_gen) * batch_size) * 100
        history_test['loss'].append(loss)
        history_test['acc'].append(accuracy)

        if history_test['acc'][-1] > best_accuracy:
            best_accuracy = history_test['acc'][-1]
            best_epoch = epoch
            print('epoch', epoch)
            check_point(model, pytorchModelFileName, epoch, optimizer, criterion)
        elif epoch - best_epoch > early_stop_thresh:
            print("Early stopped training at epoch %d" % epoch)
            break  # terminate the training loop

        if verbose or epoch + 1 == max_epochs:
            print(f'[Epoch {epoch + 1}/{max_epochs}]'
                  f" loss: {history_train['loss'][-1]:.4f}, acc: {history_train['acc'][-1]:2.2f}%"
                  f" - test_loss: {history_test['loss'][-1]:.4f}, test_acc: {history_test['acc'][-1]:2.2f}%")


        resume(model, pytorchModelFileName)

    # Generate diagnostic plots for the loss and accuracy
    fig, axes = plt.subplots(ncols=2, figsize=(9, 4.5))
    for ax, metric in zip(axes, ['loss', 'acc']):
        ax.plot(history_train[metric])
        ax.plot(history_test[metric])
        ax.set_xlabel('epoch', fontsize=12)
        ax.set_ylabel(metric, fontsize=12)
        ax.legend(['Train', 'Test'], loc='best')
        plt.savefig(os.path.abspath('..') + '/expe/out/loss_accuracy.png')
    #plt.show()

    return model



"""## 5. Simple RNN: 10 Epochs"""
"""
# Setup the training and test data generators



# Setup the RNN and training settings
input_size  = n_symbols
hidden_size = 128
output_size = n_classes
model       = SimpleRNN(input_size, hidden_size, output_size)
criterion   = torch.nn.CrossEntropyLoss()
optimizer   = torch.optim.RMSprop(model.parameters(), lr=0.001)
max_epochs  = 100

# Train the model
model = train_and_test(model, train_data_gen, dev_data_gen, criterion, optimizer, max_epochs)

for parameter_group in list(model.parameters()):
    print(parameter_group.size())

"""

"""## 6b SimpleMLP: 10 Epochs

"""
"""
# Setup the MLP and training settings
input_size  = n_symbols
output_size = n_classes
model = SimpleMLP(input_size, output_size)
criterion   = torch.nn.CrossEntropyLoss()
#optimizer   = torch.optim.Adam(model.parameters(), lr=0.01)
optimizer = torch.optim.Adamax(model.parameters(), lr=0.01)

max_epochs  = 30

# Train the model
model = train_and_test(model, train_data_gen, dev_data_gen, criterion, optimizer, max_epochs)

for parameter_group in list(model.parameters()):
    print(parameter_group.size())

"""

"""## 6. Simple LSTM: 10 Epochs"""


model       = SimpleLSTM(input_size, hidden_size, output_size)
criterion   = torch.nn.CrossEntropyLoss()
optimizer   = torch.optim.RMSprop(model.parameters(), lr=0.001)



max_epochs  = 30

# Train the model
model = train_and_test(model, train_data_gen, dev_data_gen, criterion, optimizer, max_epochs)

#for parameter_group in list(model.parameters()):
#    print(parameter_group.size())


"""## 7. Model Evaluation"""

import collections
import random

def evaluate_model(model, seed=9001, verbose=False):
    # Define a dictionary that maps class indices to labels
    #class_idx_to_label = {0: 'Q', 1: 'R', 2: 'S', 3: 'U', 4: '0', 5: '8'}
    class_idx_to_label = {n: c for n, c in enumerate(classes)}

    # Create a new data generator
    #data_generator = QRSU.prepare_data(seed=seed)
    data_generator = preprocess_data(dev_items_list, dev_labels_list, batch_size, symbol_to_idx, class_to_idx, input_size, output_size)#, n_symbols, n_classes)

    # Track the number of times a class appears
    count_classes = collections.Counter()
    # Keep correctly classified and misclassified sequences, and their
    # true and predicted class labels, for diagnostic information.
    correct = []
    incorrect = []

    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    model.eval()

    with torch.no_grad():
        for batch_idx in range(len(data_generator)):
            data, target = data_generator[batch_idx]
            data, target = torch.from_numpy(data).float().to(device), torch.from_numpy(target).long().to(device)

            data_decoded = decode_x_batch(data.cpu().numpy(), idx_to_symbol)
            target_decoded = decode_y_batch(target.cpu().numpy(), idx_to_class)

            output = model(data)
            output = output[:, -1, :]

            target = target.argmax(dim=1)
            y_pred = output.argmax(dim=1)
            y_pred_decoded = [class_idx_to_label[y.item()] for y in y_pred]

            count_classes.update(target_decoded)
            for i, (truth, prediction) in enumerate(zip(target_decoded, y_pred_decoded)):
                if truth == prediction:
                    correct.append((data_decoded[i], truth, prediction))
                else:
                    incorrect.append((data_decoded[i], truth, prediction))

    num_sequences = sum(count_classes.values())
    print('len correct', len(correct))
    print('num seqs', num_sequences)
    accuracy = float(len(correct)) / num_sequences * 100
    print(f'The accuracy of the model is measured to be {accuracy:.2f}%.\n')

    # Report the accuracy by class
    for label in sorted(count_classes):
        num_correct = sum(1 for _, truth, _ in correct if truth == label)
        #print(f'{label}: {num_correct} / {count_classes[label]} correct')

    print("Number of correct predictions: ", len(correct))

    # Report some random sequences for examination
    print('\nHere are some example sequences:')
    if len(correct) > 0:
        for i in range(0,3):
            sequence, truth, prediction = correct[random.randrange(0, 2)]
            #print(f'{sequence} -> {truth} was labelled {prediction}')

    print("Number of incorrect predictions: ", len(incorrect))

    # Report misclassified sequences for investigation
    if len(incorrect) > 0:
        print('\nThe following sequences were misclassified:')
        for i in range(0,3):
            sequence, truth, prediction = incorrect[random.randrange(0, 2)]
            print(f'{sequence} -> {truth} was labelled {prediction}')
    else:
        print('\nThere were no misclassified sequences.')

evaluate_model(model)


""" Visualize Model """

# Get hidden (H) and cell (C) batch state given a batch input (X)
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
model.eval()
with torch.no_grad():
    data = dev_data_gen[0][0]
    X = torch.from_numpy(data).float().to(device)
    H_t, C_t = model.get_states_across_time(X)

#print("Color range is as follows:")
#print_colourbar()

#plot_state(X.cpu(), C_t, b=31, decoder=decode_x, idx_to_symbol=idx_to_symbol)  # 3, 6, 9

#plot_state(X.cpu(), H_t, b=31, decoder=decode_x, idx_to_symbol=idx_to_symbol) #b=9




"""
inputSize, outputSize, x_train, y_train = readData(cffTrainFileName)
devInputSize, devOutputSize, x_dev, y_dev = readData(cffDevFileName)
model = mlp()

model = nn.Sequential()
model.add_module("dense1", nn.Linear(8,12))



model = Sequential()
model.add(Dense(units=128, activation='relu', input_dim=inputSize))
model.add(Dropout(0.4))
model.add(Dense(units=outputSize, activation='softmax'))
model.compile(loss='categorical_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])

model.fit(x_train, y_train, epochs=10, batch_size=32, validation_data=(x_dev,y_dev))


#if len(sys.argv) == 5 :
#    model.fit(x_train, y_train, epochs=5, batch_size=32, validation_data=(x_dev,y_dev))
#else :
#    model.fit(x_train, y_train, epochs=10, batch_size=32)

model.save(kerasModelFileName)
        
"""
